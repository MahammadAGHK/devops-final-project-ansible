1. playbook-docker.yml

- hosts: testserver
  roles:
    - geerlingguy.docker
    - andrewrothstein.docker-compose

Purpose: Installs Docker and Docker Compose on the specified testserver.
Roles Used:
geerlingguy.docker: Installs Docker.
andrewrothstein.docker-compose: Installs Docker Compose.

2. playbook-gitlab.yml

---
- hosts: gitlab
  become: true
  roles:
    - geerlingguy.gitlab
  vars:
    - gitlab_external_url: "https://gitlab.karabakh.pro"

Purpose: Installs GitLab on the specified hosts.
Roles Used:
geerlingguy.gitlab: Installs GitLab.
Variables:
gitlab_external_url: Sets the external URL for GitLab.

3. playbook-gnupg2.yml

- hosts: gitlab
  become: true
  pre_tasks:
    - name: Update repositories cache
      apt:
        update_cache: yes
  roles:
    - role: geerlingguy.fluentd
      tags:
        - fluentd

Purpose: Installs Fluentd on the specified GitLab hosts.
Roles Used:
geerlingguy.fluentd: Installs Fluentd.
Pre-tasks:
Updates repositories cache.

4. playbook-grafana.yml

---
- name: Install Grafana and Configure Nginx Reverse Proxy
  hosts: grafana-server
  become: yes

  tasks:
    #Tasks to download Grafana GPG key, add repository, update cache, install Grafana, start service, and enable on boot.

Purpose: Installs Grafana on the specified hosts and configures an Nginx reverse proxy.
Tasks:

- Downloads Grafana GPG key.
- Adds Grafana repository to APT sources.
- Updates APT cache.
- Installs Grafana.
- Starts Grafana service.
- Enables Grafana service on boot.

5. playbook-instance.yml

---
- name: Create instance in GCP
  #Tasks to create a disk, network, subnet, reserve a static IP, and create an instance in Google Cloud Platform (GCP).

Purpose: Creates an instance in Google Cloud Platform (GCP) with specified configurations.
Tasks:

- Creates a disk.
- Creates a network.
- Creates a subnet.
- Reserves a static IP address.
- Creates an instance.

6. playbook-kubernetes.yml

---
- name: Install Minikube and kubectl
  hosts: testserver
  become: true
  #Tasks to download and install Minikube, update apt cache, create keyring directory, download Kubernetes GPG key, and install kubectl.

Purpose: Installs Minikube and kubectl on the specified test server.
Tasks:

- Downloads and installs Minikube.
- Updates apt cache.
- Creates a keyring directory.
- Downloads Kubernetes GPG key.
- Configures Kubernetes apt repository.
- Installs kubectl.

7. playbook-prometheus.yml

---
- name: Install Prometheus on Ubuntu 22.04
  hosts: prometheus-server
  become: yes
  #Tasks to install Prometheus, create directories, download Prometheus, extract files, move binary and configuration files, create systemd service, modify configurations, reload systemd, start Prometheus service, and allow port 9090 on the firewall.

Purpose: Installs Prometheus on the specified Prometheus server.
Tasks:

- Updates system packages.
- Creates Prometheus system user and group.
- Adds Prometheus user to the group.
- Creates directories for Prometheus.
- Downloads and installs Prometheus.
- Moves binary and configuration files.
- Creates Prometheus systemd service.
- Modifies configurations.
- Reloads systemd.
- Starts Prometheus service.
- Allows port 9090 on the firewall.

8. playbook-runner.yml

---
- hosts: gitlab-runner
  become: true
  roles:
    - robertdebock.gitlab_runner
  vars_files:
    - gitlab-runner-vars.yml

Purpose: Installs GitLab Runner on the specified hosts.
Roles Used:
robertdebock.gitlab_runner: Installs GitLab Runner.
Variables:
Loads variables from gitlab-runner-vars.yml.